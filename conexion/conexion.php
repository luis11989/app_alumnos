<?php 
class DB{
    private $conexion, $result;
    
    public function DB($server, $db, $user, $pass){
        $this->conexion = mysqli_connect($server, $user, $pass, $db);
    }
    public function consulta($sql = ''){
        $this->result = mysqli_query($this->conexion, $sql) or die(mysqli_error($this->conexion));
    }
    public function obtener_respuesta(){
        return $this->result;
    }
    public function obtener_datos(){
        return $this->result->fetch_all(MYSQLI_ASSOC);
    }
}
?>