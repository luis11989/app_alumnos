$(document).ready(function(e){
    $(".appAlumnos").click(function(e){
        $("#dvAlumnos").load("modulos/alumnos/alumnos.html", function(e){
            $(this).draggable();
            $("#btnCerrarAlumnos").click(function(e){
                $("#dvAlumnos").empty();
            });
        });
    });
    $(".appMaterias").click(function(e){
        $("#dvMaterias").load("modulos/materias/materias.html", function(e){
            $(this).draggable();
            $("#btnCerrarMaterias").click(function(e){
                $("#dvMaterias").empty();
            });
        });
    });
    $(".appBuscarAlumnos").click(function(e){
        $("#dvBuscarAlumnos").load("modulos/alumnos/buscar_alumnos.html", function(e){
            $(this).draggable();
            $("#btnCerrarBuscarAlumnos").click(function(e){
                $("#dvBuscarAlumnos").empty();
            });
        });
    });
    $(".appRespaldo").click(function(e){
        $("#dvRespaldo").load("modulos/respaldo/respaldo.html", function(e){
            $(this).draggable();
            $("#btnCerrarRespaldo").click(function(e){
                $("#dvRespaldo").empty();
            });
        });
    });
    $(".rptAlumnos").click(function(e){
        $("#dvRptAlumnos").load("modulos/reporte_alumno/rptAlumnos.html", function(e){
            $(this).draggable();
            $("#btnCerrarRptAlumnos").click(function(e){
                $("#dvRptAlumnos").empty();
            });
        });
    });
});