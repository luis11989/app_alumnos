var appalumnos = new Vue({
    el: "#appAlumnos",
    data : {
        accion_user : "nuevo",
        alumnos : {
            idAlumno : "",
            codigo : "",
            nombre : "",
            direccion : "",
            telefono : "",
            activo : false
        }
    },
    methods:{
        guardarAlumnos: function(){
            var _this = this;
            
            fetch( 'modulos/alumnos/alumnos.php?accion=almacenar&accion_user='+ this.accion_user +'&datos='+ JSON.stringify(this.alumnos) )
                .then( function(r){
                    return r.json()
                })
                .then(function(respuesta){
                    if( respuesta.msg.indexOf("correcto")>=0 ){
                        _this.alumnos.codigo="";
                        _this.alumnos.nombre="";
                        _this.alumnos.direccion="";
                        _this.alumnos.telefono="";
                        _this.alumnos.activo=false;
                        _this.alumnos.idAlumnos=0;
                        _this.accion_user="nuevo";
                    }
                })
                .catch(function(e){ console.log(e); });
        }
    }
});