<?php
extract($_REQUEST);
include('../../conexion/config.php');

$alumnos = new alumnos($miConexion);
$accion = isset($accion) ? $accion : '';

if($accion==='almacenar'){
    echo $alumnos->recibir_datos($datos, $accion_user);
    
} else if($accion==='buscar'){
    echo $alumnos->buscar_datos($valor);
    
} else if($accion==='eliminar'){
    echo $alumnos->eliminar_datos($idAlumno);
}

class alumnos{
    private $datos = array(), $db;
    public $respuesta=array('msg'=>'correcto');
    
    public function __construct($miConexion=''){
        $this->db = $miConexion;
    }
    public function recibir_datos($alumnos, $accion_user=''){
        $this->datos = json_decode($alumnos, true);
        return $this->almacenar_alumnos($accion_user);
    }
    private function almacenar_alumnos($accion_user=''){
        if($accion_user==='nuevo'){
            $this->db->consulta('
                INSERT INTO alumnos (codigo,nombre,direccion,telefono,activo) VALUES(
                    "'.$this->datos['codigo'].'",
                    "'.$this->datos['nombre'].'",
                    "'.$this->datos['direccion'].'",
                    "'.$this->datos['telefono'].'",
                    "'.$this->datos['activo'].'"
                )
            ');
        } else {
            $this->db->consulta('
                UPDATE alumnos SET
                    codigo     = "'.$this->datos['codigo'].'",
                    nombre     = "'.$this->datos['nombre'].'",
                    direccion  = "'.$this->datos['direccion'].'",
                    telefono   = "'.$this->datos['telefono'].'",
                    activo     = "'.$this->datos['activo'].'"
                WHERE idAlumno = "'.$this->datos['idAlumno'].'" 
            ');
        }
        if( $this->db->obtener_respuesta() ){
            return json_encode( $this->respuesta );
        } else {
            return json_encode( $this->respuesta['msg']='error' );
        }
    }
    public function buscar_datos($valor=''){
        $this->db->consulta('
            select idAlumno, codigo, nombre, direccion, telefono, activo
            from alumnos
            where codigo like "%'.$valor.'%" or nombre like "%'.$valor.'%"
        ');
        return json_encode($this->db->obtener_datos());
    }
    public function eliminar_datos($idAlumno=0){
        $this->db->consulta('
            delete from alumnos where idAlumno='.$idAlumno
        );
        return $this->buscar_datos();
    }
}
?>