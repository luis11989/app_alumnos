var appbuscar_alumnos = new Vue({
    el: "#appBuscarAlumnos",
    data : {
        buscar : "",
        alumnos : []
    },
    methods:{
        buscarAlumnos: function(){
            var _this = this;
            
            fetch( 'modulos/alumnos/alumnos.php?accion=buscar&valor='+ this.buscar )
                .then( function(r){
                    return r.json()
                })
                .then(function(respuesta){
                    _this.alumnos = respuesta;
                    console.log(_this.alumnos);
                })
                .catch(function(e){ console.log(e); });
        }, 
        mostrarAlumno : function(alumno){
            appalumnos.alumnos.idAlumno=alumno.idAlumno;
            appalumnos.alumnos.codigo = alumno.codigo;
            appalumnos.alumnos.nombre = alumno.nombre;
            appalumnos.alumnos.direccion = alumno.direccion;
            appalumnos.alumnos.telefono = alumno.telefono;
            appalumnos.alumnos.activo = alumno.activo==1;
            appalumnos.accion_user = "modificacion";
        }, 
        eliminarAlumno : function(idAlumno){
            var _this = this;
            fetch( 'modulos/alumnos/alumnos.php?accion=eliminar&idAlumno='+ idAlumno )
                .then( function(r){
                    return r.json()
                })
                .then(function(respuesta){
                    _this.alumnos = respuesta;
                    console.log(_this.alumnos);
                })
                .catch(function(e){ console.log(e); });
        }
    },
    created : function(){
        this.buscarAlumnos();
    }
});