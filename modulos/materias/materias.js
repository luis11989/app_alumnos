var appmaterias = new Vue({
    el: "#appMaterias",
    data : {
        materias : {
            idMateria : "",
            codigo : "",
            nombre : ""
        }
    },
    methods:{
        guardarMaterias: function(){
            var _this = this;
            
            fetch( 'modulos/materias/materias.php?accion=almacenar&datos='+ JSON.stringify(this.materias) )
                .then( function(r){
                    return r.json()
                })
                .then(function(respuesta){
                    if( respuesta.msg.indexOf("correcto")>=0 ){
                        _this.materias.codigo="";
                        _this.materias.nombre="";
                        _this.materias.idMaterias=0;
                    }
                })
                .catch(function(e){ console.log(e); });
        }
    }
});