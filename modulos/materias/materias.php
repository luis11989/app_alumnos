<?php
extract($_REQUEST);
include('../../conexion/config.php');

$materias = new materias($miConexion);
$accion = isset($accion) ? $accion : '';

if($accion==='almacenar'){
    echo $materias->recibir_datos($datos);
}

class materias{
    private $datos = array(), $db;
    public $respuesta=array('msg'=>'correcto');
    
    public function __construct($miConexion=''){
        $this->db = $miConexion;
    }
    public function recibir_datos($materias){
        $this->datos = json_decode($materias, true);
        return $this->almacenar_materias();
    }
    private function almacenar_materias(){
        $this->db->consulta('
            INSERT INTO materias (codigo,nombre) VALUES(
                "'.$this->datos['codigo'].'",
                "'.$this->datos['nombre'].'"
            )
        ');
        if( $this->db->obtener_respuesta() ){
            return json_encode( $this->respuesta );
        } else {
            return json_encode( $this->respuesta['msg']='error' );
        }
    }
}
?>