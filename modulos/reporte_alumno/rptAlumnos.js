var doc_rptalumnos = new jsPDF();
var rptalumnos = new Vue({
    el: "#rptAlumnos",
    data : {
        datos : ""
    },
    methods:{
        generarReporte: function(){
            var _this = this;
            
            fetch( 'modulos/reporte_alumno/rptAlumnos.php?accion=generar_reporte' )
                .then( function(r){
                    return r.json()
                })
                .then(function(respuesta){
                    doc_rptalumnos.text(100, 20, 'Reporte de alumnos');
                    doc_rptalumnos.rect(20,30,165,200);//cuadro
                    doc_rptalumnos.line(20, 35, 185, 35);//linea horizontal x, y, w, y
                    
                    doc_rptalumnos.setFontSize(12);
                    doc_rptalumnos.text(22, 34, 'CODIGO');    
                    doc_rptalumnos.line(40, 30, 40, 230);//linea vertical x, y, x, h
                    
                    doc_rptalumnos.text(50, 34, 'NOMBRE');
                    doc_rptalumnos.line(80, 30, 80, 230);
                    
                    doc_rptalumnos.text(90, 34, 'DIRECCION');
                    doc_rptalumnos.line(135, 30, 135, 230);
                    
                    doc_rptalumnos.text(145, 34, 'TEL');
                    doc_rptalumnos.line(165, 30, 165, 230);
                   
                    doc_rptalumnos.text(167, 34, 'ACTIVO');
                    
                    var y=40;
                    respuesta.forEach(function(item, index){
                        doc_rptalumnos.text(21, y, item.codigo);
                        doc_rptalumnos.text(42, y, item.nombre);
                        doc_rptalumnos.text(82, y, item.direccion);
                        doc_rptalumnos.text(137, y, item.telefono);
                        doc_rptalumnos.text(167, y, item.activo==1 ? "SI" : "NO");
                        y+=5;
                    }); 
                    var pdf = doc_rptalumnos.output('datauristring'); 
                    $("#ifrmAlumnos").attr("src",pdf);
                })
                .catch(function(e){ console.log(e); });
        }
    }
});