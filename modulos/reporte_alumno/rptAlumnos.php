<?php
extract($_REQUEST);
include('../../conexion/config.php');

$respaldo = new respaldo($miConexion);
$accion = isset($accion) ? $accion : '';

if($accion==='generar_reporte'){
    echo $respaldo->generar_reporte();
}

class respaldo{
    private $datos = array(), $db;
    
    public function __construct($miConexion=''){
        $this->db = $miConexion;
    }
    
    public function generar_reporte(){
        $this->db->consulta('
            select idAlumno, codigo, nombre, direccion, telefono, activo
            from alumnos
        ');
        return json_encode($this->db->obtener_datos());
    }
}
?>