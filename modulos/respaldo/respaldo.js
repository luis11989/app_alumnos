var apprespaldo = new Vue({
    el: "#appRespaldo",
    data : {
        respaldo : ""
    },
    methods:{
        guardarRespaldo: function(){
            var _this = this;
            
            fetch( 'modulos/respaldo/respaldo.php' )
                .then( function(r){
                    return r.json()
                })
                .then(function(respuesta){
                    $("#tdDescargar").html(respuesta.descarga);
                })
                .catch(function(e){ console.log(e); });
        }
    }
});