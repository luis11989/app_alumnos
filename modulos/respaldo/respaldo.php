<?php
extract($_REQUEST);
include('../../conexion/config.php');

$respaldo = new respaldo($miConexion);
$accion = isset($accion) ? $accion : 'generar';

if($accion==='generar'){
    echo $respaldo->generar_respaldo();
}

class respaldo{
    public $respuesta=array('msg'=>'correcto', 'descarga'=>'');
    
    public function generar_respaldo(){
        global $server, $user, $pass, $dbase;
        $dbname = $dbase.'_'.date('dmY_H_i_s').'.sql';
        
        $mysql='"C:\xampp\mysql\bin\mysqldump.exe"';
        
        $respaldo = "$mysql --opt -h $server -u $user $dbase>$dbname";
        system($respaldo);
        
        $respuesta['descarga'] = '<a href="modulos/respaldo/'.$dbname.'">Descargar Copia</a>';
        return json_encode( $respuesta );
        
    }
}
?>